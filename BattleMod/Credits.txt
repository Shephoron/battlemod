CobaltBW:
	Lead scripting
	Game design
	Arena stage layouts
	Spritework
	Music composition/arrangement
Krabs:
	Arena stage layouts and visual designs
	v9 design & scripting
	Texture work
	Knuckles & Fang support scripting
	Beta testing
	SFX
FlareBlade93:
	Pumpkin Peak Zone stage layout and visual design
	Beta Testing
Kays:
	2D support scripting
	Object/sector respawning scripts
	Beta testing
SMS Alfredo:
	Battle shielding core script
	Damage priority support scripting
	CP HUD support scripting
Wane:
	Balance adjustments
Lianvee:
	HUD scripting
Flame:
	2D dynamic camera script
TehRealSalt:
	Original Fang popgun aim/fire re-emulation script
DirkTheHusky:
	Instashield sprites
MotorRoach:
	Tails special move sprites
Phanthonas:
	Original Tails Doll sprites
FlyingNosaj:
	Tails Doll edited sprites
Inazuma:
	Stun Break vfx sprites
Anonymous:
	Color saving for team/FFA mode switching
LJ Sonic:
	Lua assistance
	NetVars cleanup
Lat':
	UserConfig core script

7.0 Beta Testing:
	Alice
	AxelMoon
	Chrispy
	Chase
	Kays
	Krabs
	Inazuma
	Nev3r
	sphere
	

Other:
Chess pieces pulled from Secret of Mana, "Poltergeist" (spritesheet from The Spriters Resource)
Metal Sonic "claw" sprites pulled from RPG Maker 2003
Battle "shield" pulled from Sonic Battle
Various sound effects pulled from Super Smash Bros. Melee and Super Smash Bros. Ultimate (from The Sounds Resource)